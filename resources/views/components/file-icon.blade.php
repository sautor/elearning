<?php
$ext_mapping = [
    'pdf' => 'file-pdf',
    'doc' => 'file-word',
    'docx' => 'file-word',
    'xls' => 'file-excel',
    'xlsx' => 'file-excel',
    'pps' => 'file-powerpoint',
    'ppt' => 'file-powerpoint',
    'pptx' => 'file-powerpoint',
    'png' => 'file-image',
    'jpg' => 'file-image',
    'jpeg' => 'file-image',
    'gif' => 'file-image',
    'bmp' => 'file-image',
    'zip' => 'file-archive',
    'rar' => 'file-archive',
    'mp3' => 'file-audio',
];

$icon = @$extension && array_key_exists($extension, $ext_mapping) ? $ext_mapping[$extension] : 'file';
?>
<span class="sautor-list__item__logo">
  <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
    <span class="fad fa-{{ $icon }}"></span>
  </span>
</span>
