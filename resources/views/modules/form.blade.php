@include('forms.input', ['name' => 'name', 'label' => 'Nome', 'required' => true])
@include('forms.editor', ['name' => 'description', 'label' => 'Descrição', 'simple' => true, 'required' => true])
@include('forms.file', ['name' => 'image', 'label' => 'Imagem'])
@include('forms.checkbox', ['name' => 'public', 'label' => 'Público'])

<div class="form-group">
  <label class="form-control-label">
    Grupo
    @unless(\Auth::user()->hasRole('administrador')) <span class="text-danger">*</span> @endunless
  </label>
  <grupo-picker name="grupo_id" with-input value="{{ old('grupo_id', $value ?? (isset($formModel) ? $formModel['grupo_id'] : null) ?? null) }}"></grupo-picker>
</div>
