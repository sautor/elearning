@extends('elearning::layouts.main')

@php($hero = $module->image ?? Setting::get('grupo-'.$module->grupo->id.'-hero'))
@section('el.hero')
  <div class="el-hero @if($hero) has-cover @endif "
       @if($hero) style="background-image: url({{ $hero }})" @elseif($module->grupo->cor) style="background-color: {{ $module->grupo->cor }}" @endif
  >
    <div class="el-hero__body">
      <div class="container-fluid">
        <h1 class="el-hero__title">
          {{ $module->name }}
        </h1>
        <p class="el-hero__lead">
          {{ $module->grupo->nome_curto }}
        </p>
      </div>
    </div>
  </div>
@endsection

@section('el.content')

  <div class="el-section">
    <div class="container-fluid">

      <h3>
        Sobre este módulo
        @can('update', $module)
          @if($module->public)
            <span class="badge badge-info">Público</span>
          @endif
        @endcan
      </h3>
      <div class="content">
        {!! $module->description !!}
      </div>
      @can('update', $module)
        <a href="{{ route('elearning.modulos.edit', $module) }}" class="btn btn-secondary">
          <span class="fas fa-pencil"></span>
          <span>Editar</span>
        </a>
      @endcan

    </div>
  </div>

  <div class="el-section el-section--alt">
    <div class="container-fluid">

      <div class="row align-items-center mb-2">
        <h3 class="col mb-0">
          Lições
        </h3>
        <div class="col col-auto">
          <a href="{{ route('elearning.licoes.create') }}?module={{ $module->id }}" class="btn btn-secondary">
            <span class="far fa-plus"></span>
            <span class="d-none d-sm-inline">Criar</span>
          </a>
        </div>
      </div>

      @if($lessons->isNotEmpty())
        @include('elearning::lessons.list-component', ['lessons' => $lessons])
      @else
        <div class="alert alert-secondary">
          Não existem lições neste módulo.
        </div>
      @endif

      @if($notPublishedLessons->isNotEmpty())
        <h5 class="mt-4">Lições não publicadas</h5>
        @include('elearning::lessons.list-component', ['lessons' => $notPublishedLessons])
      @endif

    </div>
  </div>


@endsection
