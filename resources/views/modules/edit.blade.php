@extends('elearning::layouts.main')

@section('el.content')
  <div class="el-section">
    <div class="container">
      <h1>Editar Módulo</h1>

      @php($scope = 'edit-module')
      <form action="{{ route('elearning.modulos.update', $module) }}" method="POST" data-vv-scope="{{ $scope }}"
            enctype="multipart/form-data">
        @method('PUT')
        @csrf

        @include('elearning::modules.form')

        <div class="row">
          <div class="col">
            <button type="submit" class="btn btn-primary">Editar</button>
          </div>
          <div class="col col-auto">
            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#deleteModal">
              <span class="far fa-trash"></span>
              <span>Eliminar</span>
            </button>
          </div>
        </div>

      </form>

    </div>
  </div>


  <div class="modal" id="deleteModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <header class="modal-header">
          <h3 class="modal-title">Eliminar módulo</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </header>
        <section class="modal-body">
          <p>Tem a certeza que deseja eliminar o módulo <strong>{{ $module->name }}</strong>?</p>
        </section>
        <form action="{{ route('elearning.modulos.destroy', $module) }}" method="post" class="modal-footer">
          @csrf
          @method('DELETE')
          <button type="button" class="btn btn-link" data-toggle="modal" data-target="#deleteModal">Cancelar</button>
          <button type="submit" class="btn btn-danger">Eliminar</button>
        </form>
      </div>
    </div>
  </div>
@endsection
