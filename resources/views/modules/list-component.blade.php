<div class="modules-list">
  @if($modules->isNotEmpty())
    <div class="row">
      @foreach($modules as $module)
        <div class="column col-12 col-sm-6 col-lg-4 col-xl-3">
          <div class="card mb-3 modules-list__item">
            <div class="card-img-top">
                @if($module->image)
                  <figure class="modules-list__item__image">
                    <img src="{{ $module->image }}">
                  </figure>
                @else
                  <figure class="modules-list__item__logo">
                    <div>
                      @if($module->grupo->logo)
                        <img src="{{ $module->grupo->logo }}">
                      @else
                        <svg>
                          <use xlink:href="{{ asset('img/sprite.svg') }}#igreja"/>
                        </svg>
                      @endif
                    </div>
                  </figure>
                @endif
            </div>
            <div class="card-body modules-list__item__body">
              <div class="media">
                <div class="media-body">
                  <h5 class="mb-0">{{ $module->name }}</h5>
                  <p class="text-muted">{{ $module->grupo->nome_curto }}</p>
                </div>
                @if($module->grupo->logo)
                  <img src="{{ $module->grupo->logo }}" class="ml-3" style="width: 48px;height: 48px;">
                @endif
              </div>

              <div class="modules-list__item__desc">
                {!! $module->description !!}
              </div>
              <a href="{{ route('elearning.modulos.show', $module) }}" class="card-link">
                Ver módulo
                <span class="far fa-arrow-right"></span>
              </a>

            </div>
          </div>
        </div>
      @endforeach
    </div>
  @else
    <div class="alert alert-secondary mb-0">
      {{ empty($empty_message) ? 'Não existem módulos.' : $empty_message }}
    </div>
  @endif
</div>
