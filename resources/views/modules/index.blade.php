@extends('elearning::layouts.main')


@section('el.hero')
  <div class="el-hero has-main-bg has-cover">
    <div class="el-hero__body">
      <div class="container-fluid">
        @auth
          <h1 class="el-hero__title">
            Olá {{ Auth::user()->nome_curto }}!
          </h1>
          <p class="el-hero__lead">
            {{ Auth::user()->genero === 'f' ? 'Bem-vinda' : 'Bem-vindo' }} ao {{ config('app.name') }} eLearning
          </p>
        @endauth
        @guest
          <h1 class="el-hero__title">
            Bem-vindo ao eLearning!
          </h1>
        @endguest
      </div>
    </div>
  </div>
@endsection

@section('el.content')
  <div class="el-section">
    <div class="container-fluid">
      <h2>Módulos públicos</h2>
      @include('elearning::modules.list-component', ['modules' => $publicModules])
    </div>
  </div>

  @auth
    <div class="el-section el-section--alt">
      <div class="container-fluid">
        <h2>Módulos dos meus grupos</h2>

        @unless(empty($modulesFromMyGroups))
          @include('elearning::modules.list-component', [ 'modules' => $modulesFromMyGroups ])
        @else
          <div class="alert alert-secondary">
            Não tem grupos com módulos.
          </div>
        @endunless

        @can('create', \Sautor\ELearning\Models\Module::class)
          <a href="{{ route('elearning.modulos.create') }}" class="btn btn-secondary">
            <span class="far fa-plus"></span>
            <span>Criar módulo</span>
          </a>
        @endcan


        @if($myGroups->isNotEmpty())
          <div class="el-grupos">
              <h3>Os meus grupos</h3>
              <div class="el-grupos__container">
                @foreach($myGroups as $grupo)
                  <a href="{{ route('elearning.grupo', $grupo) }}" class="el-grupos__grupo">
                    @if($grupo->logo)
                      <img class="el-grupos__grupo__logo" src="{{$grupo->logo}}" alt="{{$grupo->nome}}">
                    @else
                      <span class="el-grupos__grupo__nome">{{$grupo->nome}}</span>
                    @endif
                  </a>
                @endforeach
              </div>
          </div>
        @endif

      </div>
    </div>
  @endauth

@endsection
