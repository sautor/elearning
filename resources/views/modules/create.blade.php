@extends('elearning::layouts.main')

@section('el.content')
  <div class="el-section">
    <div class="container">
      <h1>Criar Módulo</h1>

      @php($scope = 'new-module')
      <form action="{{ route('elearning.modulos.store') }}" method="POST" data-vv-scope="{{ $scope }}"
            enctype="multipart/form-data">
        {{ csrf_field() }}

        @include('elearning::modules.form')

        <button class="btn btn-primary">Criar</button>

      </form>
    </div>
  </div>
@endsection
