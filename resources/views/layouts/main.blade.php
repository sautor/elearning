@extends('layouts.basic')
@section('content')
  @include('elearning::layouts.partials.header')
  @yield('el.hero')
  @yield('el.content')
@endsection
