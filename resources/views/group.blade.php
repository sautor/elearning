@extends('elearning::layouts.main')

@php($hero = Setting::get('grupo-'.$grupo->id.'-hero'))
@section('el.hero')
  <div class="el-hero el-hero--group"
       @if($hero) style="background-image: url({{ $hero }})" @endif
  >
    <div class="el-hero__body">
      <div class="container-fluid">
        @if($grupo->logo)
          <img src="{{ $grupo->logo }}" alt="{{ $grupo->nome_curto }}" class="el-hero__logo">
        @else
          <h1 class="el-hero__title">
            {{ $grupo->nome }}
          </h1>
        @endif
      </div>
    </div>
  </div>
@endsection

@section('el.content')

  <div class="el-section">
    <div class="container-fluid">

      @if($grupo->logo)
        <h1>{{ $grupo->nome }}</h1>
      @endunless

        <h2>Módulos públicos</h2>

        @include('elearning::modules.list-component', ['modules' => $publicModules])

    </div>
  </div>

  @can('details', $grupo)
    <div class="el-section el-section--alt">
      <div class="container-fluid">

        <h2>Módulos não públicos</h2>

        @include('elearning::modules.list-component', ['modules' => $privateModules])

        @can('create', \Sautor\ELearning\Models\Module::class)
          @can('update', $grupo)
            <a href="{{ route('elearning.modulos.create') }}?grupo={{ $grupo->id }}" class="btn btn-secondary">
              <span class="far fa-plus"></span>
              <span>Criar módulo</span>
            </a>
          @endcan
        @endcan

      </div>
    </div>
  @endcan

@endsection
