<div class="lessons-list">
@foreach($lessons as $lesson)
  @php($completion = $lesson->completedBy()->where('pessoa_id', Auth::id())->first())
  @php($lessonTag = $lesson->is_available || Gate::allows('update', $lesson) ? 'a' : 'div')
  <{{ $lessonTag }} @if($lessonTag === 'a') href="{{ route('elearning.licoes.show', $lesson) }}" @endif class="lessons-list__item">

    @if($lesson->image)
      <img src="{{ $lesson->image }}" class="lessons-list__item__image">
    @else
      <div class="lessons-list__item__icon">
        <span class="fas fa-book fa-2x"></span>
      </div>
    @endif

    <div class="lessons-list__item__data">
      <h5 class="lessons-list__item__title">{{ $lesson->title }}</h5>
      <div class="lessons-list__item__desc">
        {!! $lesson->description !!}
      </div>
      @if(!$lesson->is_published)
        <small class="text-muted">
          Esta lição será publicada
          <time title="{{ $lesson->published_at->isoFormat('LLL') }}">{{ $lesson->published_at->diffForHumans() }}</time>
        </small>
      @endif
      @if(!$lesson->is_available)
        <small class="text-muted">
          Esta lição estará disponível
          <time title="{{ $lesson->available_at->isoFormat('LLL') }}">{{ $lesson->available_at->diffForHumans() }}</time>
        </small>
      @endif
        <div class="lessons-list__item__meta">
          @if($lesson->video)
            <span class="lessons-list__item__meta__item">
              <span class="fas fa-film"></span>
            </span>
          @endif
          @if($lesson->audio)
            <span class="lessons-list__item__meta__item">
              <span class="fas fa-volume"></span>
            </span>
          @endif
          @unless(empty($lesson->content))
            <span class="lessons-list__item__meta__item">
              <span class="fas fa-align-left"></span>
            </span>
          @endunless
          @if($lesson->quiz)
            <span class="lessons-list__item__meta__item">
              @include('elearning::partials.icon-catequiz')
              @if($completion && $completion->completion->score !== null)
                <span class="badge badge-light ml-1">{{ $completion->completion->score }}</span>
              @endif
            </span>
          @endif
        </div>
    </div>
    @auth
      @if($completion)
        <div class="lessons-list__item__complete">
          @if($lesson->quiz && $completion->completion->score === null)
            <span class="text-warning">
              <span class="far fa-check-circle fa-lg"></span>
            </span>
          @else
            <span class="text-success">
              <span class="fas fa-check-circle fa-lg"></span>
            </span>
          @endif
        </div>
      @endif
    @endauth
  </{{ $lessonTag }}>
@endforeach
</div>
