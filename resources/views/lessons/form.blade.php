@include('forms.input', ['name' => 'title', 'label' => 'Título', 'required' => true])
@include('forms.editor', ['name' => 'description', 'label' => 'Descrição', 'required' => true, 'simple' => true])
@include('forms.editor', ['name' => 'content', 'label' => 'Conteúdo'])
@include('forms.file', ['name' => 'image', 'label' => 'Imagem', 'options' => ['accept' => 'image/*']])
@include('forms.file', ['name' => 'audio', 'label' => 'Ficheiro áudio', 'options' => ['accept' => '.mp3']])
@include('forms.input', ['name' => 'video', 'label' => 'URL de vídeo', 'options' => ['type' => 'url']])
@include('forms.file', ['name' => 'quiz', 'label' => 'Ficheiro de quiz', 'help' => 'O ficheiro de quiz pode ser gerado no <a href="https://catequiz.gitlab.io/creator/#/mini" target="_blank">Catequiz Creator</a>', 'options' => ['accept' => '.json']])
@include('forms.checkbox', ['name' => 'public', 'label' => 'Público'])

<div class="row">
  <div class="col col-sm">
    @include('forms.select', ['name' => 'module_id', 'label' => 'Módulo', 'required' => true, 'list' => $modules->pluck('name', 'id'), 'value' => @$module_id])
  </div>
  <div class="col col-sm">
    @include('forms.input', ['name' => 'order', 'label' => 'Ordem no módulo', 'required' => true, 'options' => ['pattern' => '[0-9]*', 'inputtype' => 'numeric']])
  </div>
</div>

<div class="row">
  <div class="col col-sm">
    @include('forms.input', ['name' => 'published_at', 'label' => 'Data de publicação', 'value' => isset($lesson) && $lesson->published_at ? $lesson->published_at->isoFormat('YYYY-MM-DD[T]HH:mm') : null, 'options' => ['type' => 'datetime-local'], 'help' => 'A lição será visível a partir desta data e hora. Se o campo for deixado em branco, será visível imediatamente.'])
  </div>
  <div class="col col-sm">
    @include('forms.input', ['name' => 'available_at', 'label' => 'Disponível a partir de', 'options' => ['type' => 'datetime-local'], 'value' => isset($lesson) && $lesson->available_at ? $lesson->available_at->isoFormat('YYYY-MM-DD[T]HH:mm') : null, 'help' => 'O conteúdo da lição será visível a partir desta data. Se o campo for deixado em branco, será visível assim que publicada.'])
  </div>
</div>
