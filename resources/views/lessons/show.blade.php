@extends('elearning::layouts.main')

@php($hero = $lesson->image ?? $lesson->module->image ?? Setting::get('grupo-'.$lesson->grupo->id.'-hero'))
@section('el.hero')
  <div class="el-hero @if($hero) has-cover @endif "
       @if($hero) style="background-image: url({{ $hero }})" @elseif($lesson->grupo->cor) style="background-color: {{ $lesson->grupo->cor }}" @endif
  >
    <div class="el-hero__body">
      <div class="container-fluid">
        <h1 class="el-hero__title">
          {{ $lesson->title }}
        </h1>
        <p class="el-hero__desc">
          {!! $lesson->description !!}
        </p>
        @can('update', $lesson)
          <a href="{{ route('elearning.licoes.edit', $lesson) }}" class="btn btn-sm btn-outline-light">
            <span class="fas fa-pencil"></span>
            <span>Editar</span>
          </a>
        @endcan

      </div>
    </div>
  </div>
@endsection

@section('el.content')

  <div class="el-section">
    <div class="container">

      @if(!$lesson->is_published)
        <div class="alert alert-info">
          <h5 class="alert-heading">
            Esta lição não está publicada.
          </h5>
          <p class="mb-0">
            Esta lição será publicada apenas {{ $lesson->published_at->isoFormat('LLLL') }}.
            Até lá, não aparecerá na lista das lições do curso e não poderá ser vista.
          </p>
        </div>
      @elseif(!$lesson->is_available)
        <div class="alert alert-info">
          <h5 class="alert-heading">
            Esta lição não está disponível.
          </h5>
          <p class="mb-0">
            Esta lição ficará disponível apenas a partir de {{ $lesson->available_at->isoFormat('LLLL') }}.
            Até lá, aparecerá na lista das lições do curso mas o seu conteúdo não poderá ser visto.
          </p>
        </div>
      @endif

      @include('elearning::lessons.partials.content')
      @include('elearning::lessons.partials.attachments')


        <div class="row mt-5">
          <div class="col">
            <a href="{{ route('elearning.modulos.show', $lesson->module) }}" class="btn btn-secondary">
              <span class="far fa-arrow-left"></span>
              <span>Voltar ao módulo</span>
            </a>
          </div>
          <div class="col col-auto">
            @auth

              @if($lesson->quiz && $completion && $completion->score !== null)
                <a href="{{ route('elearning.licoes.showQuiz', $lesson) }}" class="btn btn-secondary">
                  @include('elearning::partials.icon-catequiz')
                  <span>Repetir o quiz</span>
                  <span class="far fa-arrow-right"></span>
                </a>
              @elseif($lesson->quiz && $completion)
                <a href="{{ route('elearning.licoes.showQuiz', $lesson) }}" class="btn btn-secondary">
                  @include('elearning::partials.icon-catequiz')
                  <span>Avançar para o quiz</span>
                  <span class="far fa-arrow-right"></span>
                </a>
              @elseif($lesson->quiz)
                <form action="{{ route('elearning.licoes.storeCompletion', $lesson) }}" method="POST">
                  @csrf
                  <button class="btn btn-primary">
                    @include('elearning::partials.icon-catequiz')
                    <span>Terminar e avançar para o quiz</span>
                    <span class="far fa-arrow-right"></span>
                  </button>
                </form>
              @elseif(!$completion)
                <form action="{{ route('elearning.licoes.storeCompletion', $lesson) }}" method="POST">
                  @csrf
                  <button class="btn btn-success">
                    <span>Terminar lição</span>
                    <span class="far fa-arrow-right"></span>
                  </button>
                </form>
              @endif

            @endauth

            @guest
              @if($lesson->quiz)
                <a href="{{ route('elearning.licoes.showQuiz', $lesson) }}" class="btn btn-secondary">
                  @include('elearning::partials.icon-catequiz')
                  <span>Avançar para o quiz</span>
                  <span class="far fa-arrow-right"></span>
                </a>
              @endif
            @endguest
          </div>
        </div>

    </div>
  </div>

  <div class="el-section el-section--alt">
    <div class="container-fluid">

      @can('update', $lesson)
        <h4>Já completaram esta lição</h4>

        @component('pessoas.list-component', [
                                'pessoas' => $lesson->completedBy,
                                'meta' => function($p) {
                                    $meta = $p->completion->created_at->tz('Europe/Lisbon')->isoFormat('LLL');
                                    if ($p->completion->score !== null) {
                                      $meta .= ' &middot; ';
                                      $meta .= view('elearning::partials.icon-catequiz');
                                      $meta .= ' '.$p->completion->score;
                                    }
                                    return $meta;
                                },
                                'empty_message' => 'Ninguém completou esta lição'
                                ])
        @endcomponent
      @endcan

    </div>
  </div>


@endsection

