@extends('elearning::layouts.main')

@php($hero = $lesson->image ?? $lesson->module->image ?? Setting::get('grupo-'.$lesson->grupo->id.'-hero'))
@section('el.hero')
  <div class="el-hero @if($hero) has-cover @endif "
       @if($hero) style="background-image: url({{ $hero }})" @elseif($lesson->grupo->cor) style="background-color: {{ $lesson->grupo->cor }}" @endif
  >
    <div class="el-hero__body">
      @include('elearning::partials.cq-bg')
      <div class="container-fluid">
        <h1 class="el-hero__title mb-2">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 487 55.79" fill="currentColor" style="height: 3rem; max-width:100%" class="has-margin-bottom-md">
            <path d="M68.64 37.67a28.72 28.72 0 01-3.92 6.56 19.57 19.57 0 01-3.65 3.68c-.54.4-.86.36-1.23-.18-2.2-3.27-4.4-6.56-6.6-9.84a1.31 1.31 0 01-.13-.23h-3.88a.69.69 0 01.41.32q3.78 5.66 7.58 11.3c.51.75.45 1-.33 1.42a26.5 26.5 0 01-23.3.63 5.19 5.19 0 01-1.43-.89.69.69 0 01-.12-1q3.87-5.73 7.72-11.48a.62.62 0 01.34-.27h-3.82c-.28.41-.51.77-.74 1.12l-5.89 8.75c-.46.68-.73.75-1.44.29a13.18 13.18 0 01-2.79-2.69A25.1 25.1 0 0121 38.25a1.33 1.33 0 01-.19-.59h-2.75c.31.05.38.23.6.81A24.47 24.47 0 0027 49.4a31.53 31.53 0 0020.72 6.35 23.28 23.28 0 006.46-1.21A25 25 0 0065 47.27a29.43 29.43 0 005.49-9.09c.11-.3.24-.46.48-.52h-2.33zM89.25 19.4a1.3 1.3 0 00-1.41-1.24h-16.3a.76.76 0 01-.76-.61 23.26 23.26 0 00-1.53-3.33 24.24 24.24 0 00-4.16-5.53 28.21 28.21 0 00-9.18-6.07A30.88 30.88 0 0039.37.29a24.15 24.15 0 00-13 6.16 28.35 28.35 0 00-7.54 11.19.71.71 0 01-.72.55H1.32c-.9 0-1.32.42-1.32 1.3v16.86c0 .77.32 1.16 1 1.26a1.5 1.5 0 00.37 0H87.9a1.18 1.18 0 00.46-.09h.08a1.19 1.19 0 00.76-.84 1.34 1.34 0 00.06-.4V19.55a.76.76 0 00-.01-.15zm-68.49-1.23a4.46 4.46 0 01.52-1.25 27.66 27.66 0 016.1-8.33c.28-.25.58-.48.88-.71.55-.4.87-.35 1.26.23 1.56 2.31 3.11 4.63 4.66 6.94l2.11 3.12zm28.2 0h-8.59a.72.72 0 01-.68-.38c-2.52-3.77-5.06-7.54-7.58-11.3-.49-.73-.44-1 .35-1.41a25.32 25.32 0 017-2.49 25.52 25.52 0 0116 1.77c.52.25 1 .5 1.56.79s.7.64.28 1.27q-3.82 5.7-7.65 11.39a.72.72 0 01-.65.38zm19.27 0H53.07l2.6-3.88 4.1-6.09c.45-.67.73-.73 1.37-.22a24.55 24.55 0 017.32 9.8c.11.28.14.41-.23.41zM139.09 40.86a12.51 12.51 0 01-12.93-12.93A12.51 12.51 0 01139.09 15a13 13 0 019.26 3.6l10.13-10.21a27.21 27.21 0 00-19.39-8 27.55 27.55 0 100 55.1 28.12 28.12 0 0019.54-8.16l-10.28-10.14a12.63 12.63 0 01-9.26 3.67zM186.29.38l-25.71 55.1H175l3.31-7.2h22.55l3.38 7.2h14.47L192.75.38zm-2.35 35.7L189.59 24l5.59 12.12zM211.67 14.71H226v40.77h14.47V14.71h14.62V.31h-43.42v14.4zM262.18 55.48h34.23V43.65h-19.76V33.51h17.48V21.32h-17.48v-8.96h19.76V.38h-34.23v55.1zM329.58.38C312.9.38 302 12.72 302 27.93a27.21 27.21 0 0027 27.55 27.94 27.94 0 0014.48-3.9l3.89 3.9h9.7V45l-3.68-3.74a25.67 25.67 0 003.68-13.3C357.13 12.72 345.59.38 329.58.38zm12.85 30.12L336 24l-10.06 9.92 6.75 6.68a13.59 13.59 0 01-3.08.37A13 13 0 11342.73 28a13.36 13.36 0 01-.3 2.5zM392.42 33.51a7.2 7.2 0 11-14.4 0V.38h-14.54v33.35a21.75 21.75 0 1043.49 0V.38h-14.55zM415.89.38h14.47v55.1h-14.47zM463.49 41.01L486.93.38h-48.56v14.55h23.51l-23.44 40.55H487V41.01h-23.51z"/>
          </svg>
        </h1>
        <p class="el-hero__lead">
          {{ $lesson->title }}
        </p>
      </div>
    </div>
  </div>
@endsection

@section('el.content')

  <div class="el-section">
    <div class="container">

      @if($completion && $completion->score !== null)
        <article class="alert alert-info mb-5">
          Já completaste este quiz, com uma pontuação de <strong>{{ $completion->score }}</strong>.
          Esta tentativa não será contabilizada.
        </article>
      @endif

      <mini-catequiz path="{{ $lesson->quiz }}" color="{{ $lesson->grupo->cor ?: 'var(--primary)' }}" font="var(--font-family)" single-try></mini-catequiz>

      <div class="row">
        <div class="col">
          <a href="{{ route('elearning.licoes.show', $lesson) }}" class="btn btn-secondary">
            <span class="far fa-arrow-left"></span>
            <span>
            Voltar à lição
          </span>
          </a>
        </div>
        <div class="col-auto">
          @if(\Auth::guest() || ($completion && $completion->score !== null))
            <a href="{{ route('elearning.modulos.show', $lesson->module) }}" class="btn btn-secondary">
              Voltar ao módulo
            </a>
          @else
            <form action="{{ route('elearning.licoes.storeScore', $lesson) }}" method="POST" id="scoreForm">
              @csrf
              <input type="hidden" name="score" />
              <button class="btn btn-success" disabled>
                <span class="far fa-check"></span>
                <span>
                Terminar lição
              </span>
              </button>
            </form>
          @endif
        </div>
      </div>

    </div>
  </div>

@endsection

@push('scripts')
  <script src="{{ asset('vendor/elearning/js/mini-catequiz.min.js') }}"></script>
  <script>
    const mc = document.querySelector('mini-catequiz');
    const scoreForm = document.querySelector('#scoreForm')
    if (scoreForm) {
      mc.addEventListener('completed', e => {
        const score = Math.ceil((e.detail[0].score / e.detail[0].questions) * 100)
        scoreForm.querySelector('input[name=score]').value = score
        scoreForm.querySelector('button').disabled = false
      })
    }
  </script>
@endpush
