@extends('elearning::layouts.main')

@section('el.content')
  <div class="el-section">
    <div class="container">
      <h1>Criar Lição</h1>

      @php($scope = 'new-lesson')
      <form action="{{ route('elearning.licoes.store') }}" method="POST" data-vv-scope="{{ $scope }}"
            enctype="multipart/form-data">
        {{ csrf_field() }}

        @include('elearning::lessons.form')

        <button class="btn btn-primary">Criar</button>
      </form>
    </div>
  </div>
@endsection
