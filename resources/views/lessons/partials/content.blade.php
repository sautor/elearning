@if($lesson->video)
  @php($videoInfo = \Embed\Embed::create($lesson->video))
  <div class="mb-5">
    @if($videoInfo->providerName === 'YouTube')
      <div data-player data-plyr-provider="youtube" data-plyr-embed-id="{{ $videoInfo->url }}"></div>
      <small class="text-center text-muted mt-2 d-block">
        <a href="{{ $videoInfo->url }}">{{ $videoInfo->title }}</a>
        em <span class="fab fa-youtube"></span> YouTube
      </small>
    @else
      <div class="plyr__video-embed" data-player>
        {!! $videoInfo->code !!}
      </div>
      <small class="text-center text-muted mt-2 d-block">
        <a href="{{ $videoInfo->url }}">{{ $videoInfo->title }}</a>
        em {{ $videoInfo->providerName }}
      </small>
    @endif
  </div>
@endif

@if($lesson->audio)
  <div class="mb-5">
    <audio src="{{ $lesson->audio }}" controls data-player></audio>
  </div>
@endif

@if($lesson->content)
  <div class="content mb-5">
    {!! $lesson->content !!}
  </div>
@endif

@if($lesson->video or $lesson->audio)
  @push('scripts')
    <script src="{{ asset('vendor/elearning/js/plyr.min.js') }}"></script>
    <script>
      const players = document.querySelectorAll('[data-player]')
      Plyr.setup(players)
    </script>
  @endpush
@endif
