@if($lesson->hasMedia() || Gate::allows('update', $lesson))

  <div class="row align-items-center mb-3">
    <h2 class="col mb-0">
      Anexos
    </h2>
    @can('update', $lesson)
      <div class="col col-auto">
        <button class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#addAttachmentModal">
          <span class="far fa-plus"></span>
          <span class="d-none d-sm-inline">Adicionar</span>
        </button>
      </div>
    @endcan
  </div>

  @if($errors->isNotEmpty())
    <div class="alert alert-danger">
      Ocorreram alguns erros ao adicionar um anexo:
      <ul>
        @foreach($errors->all() as $message)
          <li>{{ $message }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  @if($lesson->getMedia()->isNotEmpty())
    <div class="sautor-list">
      @foreach($lesson->getMedia() as $media)
        <div class="sautor-list__item">
          @include('elearning::components.file-icon', ['class' => 'mr-2', 'extension' => $media->extension])
          <a href="{{ $media->getUrl() }}" class="sautor-list__item__data">
            <p class="sautor-list__item__name">
              {{ $media->name }}
            </p>
            <p class="sautor-list__item__meta">
              {{ strtoupper($media->extension) }}
              &middot;
              {{ $media->human_readable_size }}
            </p>
          </a>
          <div class="sautor-list__item__actions">
            <a href="{{ $media->getUrl() }}">
              <span class="far fa-cloud-download"></span>
            </a>
            @can('update', $lesson)
              <a href="#" data-toggle="modal" data-target="#delete{{ $media->id }}Modal">
                <span class="far fa-trash"></span>
              </a>
            @endcan
          </div>
        </div>
      @endforeach
    </div>

  @else

    <section class="sautor-list--empty">
      Esta lição não tem anexos.
    </section>

  @endif

@endif

@can('update', $lesson)

  <div class="modal" id="addAttachmentModal">
    <div class="modal-dialog">
      <form class="modal-content" method="POST" enctype="multipart/form-data" action="{{ route('elearning.licoes.addAttachment', $lesson) }}">
        @csrf
        <header class="modal-header">
          <h3 class="modal-title">Adicionar anexo</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </header>
        <section class="modal-body">
          @include('forms.file', ['name' => 'file', 'label' => 'Ficheiro', 'required' => true])
          @include('forms.input', ['name' => 'name', 'label' => 'Nome', 'help' => 'Por omissão, o anexo terá o nome do ficheiro.'])
        </section>
        <footer class="modal-footer">
          <button class="btn btn-link" type="button" data-toggle="modal" data-target="#addAttachmentModal">Cancelar</button>
          <button class="btn btn-success" type="submit">Adicionar</button>
        </footer>
      </form>
    </div>
  </div>

  @foreach($lesson->getMedia() as $media)
    <div class="modal" id="delete{{ $media->id }}Modal">
      <div class="modal-dialog">
        <form class="modal-content" method="POST" action="{{ route('elearning.licoes.deleteAttachment', [$lesson, $media]) }}">
          @csrf
          @method('DELETE')
          <header class="modal-header">
            <h3 class="modal-title">Eliminar anexo</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
              <span aria-hidden="true">&times;</span>
            </button>
          </header>
          <section class="modal-body">
            <p>Tem a certeza que quer eliminar o anexo <strong>{{ $media->file_name }}</strong> ?</p>
            <p class="text-danger">
              <strong>Atenção!</strong> A eliminação é permanente.
            </p>
          </section>
          <footer class="modal-footer">
            <button class="btn btn-link" type="button" data-toggle="modal" data-target="#delete{{ $media->id }}Modal">Cancelar</button>
            <button class="btn btn-danger" type="submit">Eliminar</button>
          </footer>
        </form>
      </div>
    </div>
  @endforeach
@endcan
