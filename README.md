# Sautor eLearning

Extensão de eLearning para o sistema Sautor.

## Instalação

Instalar via Composer:

````sh
composer require sautor/elearning
````

Copiar os recursos necessários:

````sh
php artisan vendor:publish --tag=elearning.assets
````

No final do ficheiro `resources/scss/app.scss`:

````scss
// Addons
@import "@sautor/elearning/resources/scss/styles";
````

Instalar as dependências JS necessárias:

````sh
npm i plyr
````
