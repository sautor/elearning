<?php


namespace Sautor\ELearning\Traits;


use Sautor\ELearning\Models\Module;

trait HasModules {

    public function modules()
    {
        return $this->hasMany(Module::class);
    }

}
