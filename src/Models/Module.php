<?php

namespace Sautor\ELearning\Models;

use Sautor\Core\Models\Grupo;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lms_modules';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

	public function scopePublic(Builder $query)
	{
		return $query->where('public', true);
	}

    public function lessons()
    {
        return $this->hasMany(Lesson::class)->orderBy('order');
    }

    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
