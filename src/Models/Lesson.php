<?php

namespace Sautor\ELearning\Models;

use Sautor\Core\Models\Pessoa;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Lesson extends Model implements HasMedia
{
    use SoftDeletes, Sluggable, InteractsWithMedia;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lms_lessons';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'published_at',
        'available_at',
    ];

	/**
	 * All of the relationships to be touched.
	 *
	 * @var array
	 */
	protected $touches = ['module'];

    public function scopePublished(Builder $query)
    {
        return $query->whereDate('published_at', '<=', Carbon::now()->toDateTimeString())->orWhereNull('published_at');
    }

    public function scopeNotPublished(Builder $query)
    {
        return $query->whereDate('published_at', '>', Carbon::now()->toDateTimeString());
    }

    public function getIsPublishedAttribute()
    {
        return !$this->published_at || $this->published_at->isBefore(Carbon::now());
    }

    public function getIsAvailableAttribute()
    {
        return !$this->available_at || $this->available_at->isBefore(Carbon::now());
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function grupo()
    {
        return $this->module->grupo();
    }

    public function completedBy()
    {
        return $this->belongsToMany(Pessoa::class, 'lms_lesson_completions')
            ->as('completion')->withPivot('score')->withTimestamps();
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
