<?php

namespace Sautor\ELearning\Policies;

use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Pessoa;
use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\ELearning\Models\Module;

class ModulesPolicy
{
    use HandlesAuthorization;

    public function before(Pessoa $user, $ability){
        // TODO: Proper permission
        if ($user->hasRole('administrador')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \Sautor\Core\Models\Pessoa  $user
     * @return mixed
     */
    public function viewAny(?Pessoa $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \Sautor\Core\Models\Pessoa  $user
     * @param  \Sautor\ELearning\Models\Module  $module
     * @return mixed
     */
    public function view(?Pessoa $user, Module $module)
    {
        if($module->public) return true;
        if(!$user) return false;
        if(empty($module->grupo)) return false;
        return $user->can('details', $module->grupo);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \Sautor\Core\Models\Pessoa  $user
     * @return mixed
     */
    public function create(Pessoa $user)
    {
        return $user->can('create', Grupo::class);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \Sautor\Core\Models\Pessoa  $user
     * @param  \Sautor\ELearning\Models\Module  $module
     * @return mixed
     */
    public function update(Pessoa $user, Module $module)
    {
        return $module->grupo && $user->can('update', $module->grupo);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \Sautor\Core\Models\Pessoa  $user
     * @param  \Sautor\ELearning\Models\Module  $module
     * @return mixed
     */
    public function delete(Pessoa $user, Module $module)
    {
        return $user->can('update', $module);
    }
}
