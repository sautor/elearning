<?php

namespace Sautor\ELearning\Policies;

use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Pessoa;
use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\ELearning\Models\Lesson;

class LessonsPolicy
{
    use HandlesAuthorization;

    public function before(Pessoa $user, $ability){
        // TODO: Proper permission
        if ($user->hasRole('administrador')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \Sautor\Core\Models\Pessoa  $user
     * @return mixed
     */
    public function viewAny(?Pessoa $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \Sautor\Core\Models\Pessoa $user
     * @param Lesson $lesson
     * @return mixed
     */
    public function view(?Pessoa $user, Lesson $lesson)
    {
        if((!$lesson->is_published || !$lesson->is_available) && !\Gate::allows('update', $lesson)) return false;
        if($lesson->public && $lesson->module->public) return true;
        if(!$user) return false;
        if(empty($lesson->grupo)) return false;
        return $user->can('details', $lesson->grupo);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \Sautor\Core\Models\Pessoa  $user
     * @return mixed
     */
    public function create(Pessoa $user)
    {
        return $user->can('create', Grupo::class);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \Sautor\Core\Models\Pessoa $user
     * @param Lesson $lesson
     * @return mixed
     */
    public function update(Pessoa $user, Lesson $lesson)
    {
        return $lesson->grupo && $user->can('update', $lesson->grupo);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \Sautor\Core\Models\Pessoa $user
     * @param Lesson $lesson
     * @return mixed
     */
    public function delete(Pessoa $user, Lesson $lesson)
    {
        return $user->can('update', $lesson);
    }
}
