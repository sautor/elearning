<?php

namespace Sautor\ELearning;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Sautor\ELearning\Models\Lesson;
use Sautor\ELearning\Models\Module;
use Sautor\ELearning\Policies\LessonsPolicy;
use Sautor\ELearning\Policies\ModulesPolicy;
use Sautor\Core\Services\Addons\Addon;
use Sautor\Core\Models\Grupo;

class ELearningServiceProvider extends ServiceProvider
{

    protected $policies = [
        Module::class => ModulesPolicy::class,
        Lesson::class => LessonsPolicy::class,
    ];

	private Addon $addon;

	public function __construct($app) {
		parent::__construct($app);
		$this->addon = Addon::create('elearning', 'eLearning', 'graduation-cap', 'Esta extensão adiciona ao sistema um módulo de eLearning.')
			->setGroupAddon(false)
			->setManagersOnly(false)
		    ->setEntryRoute('elearning.index')
			->setEntryRouteForGroup('elearning.grupo')
			->setRestrict(function (?Grupo $grupo) {
				if ($grupo) {
					return Module::where('grupo_id', $grupo->id)->public()->doesntExist();
				}
				return Module::public()->doesntExist();
			});

	}

	/**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'joaopluis');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'elearning');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        $this->registerPolicies();

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }

	    $addonService = $this->app->make('AddonService');
	    $addonService->register($this->addon);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/elearning.php', 'elearning');
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/elearning.php' => config_path('elearning.php'),
        ], 'elearning.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/elearning'),
        ], 'elearning.views');*/

        // Publishing assets.
        $this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/elearning'),
        ], 'elearning.assets');

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/elearning'),
        ], 'elearning.views');*/

        // Registering package commands.
        // $this->commands([]);
    }

    public function registerPolicies()
    {
        foreach ($this->policies as $key => $value) {
            Gate::policy($key, $value);
        }
    }
}
