<?php

namespace Sautor\ELearning\Controllers;

use Sautor\Core\Models\Grupo;
use Sautor\Core\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Sautor\ELearning\Models\Module;

class ModulesController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Module::class, 'module');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $publicModules = Module::where('public', true)->get();
        $modulesFromMyGroups = [];
        $myGroups = [];
        $addon = \AddonService::get('elearning');
        if(\Auth::user()) {
        	$modulesFromMyGroups = Module::latest()->whereIn('grupo_id', \Auth::user()->grupos->pluck('id'))->limit(12)->get();
            $myGroups = \Auth::user()->grupos->filter(function (Grupo $grupo) use ($addon) { return $addon->isEnabledFor($grupo); });
        }
        return view('elearning::modules.index', compact('publicModules', 'modulesFromMyGroups', 'myGroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $user = Auth::user();
        $managedGroups = Grupo::all()->filter(function($g) use ( $user ) {
            return $g->isManagedBy($user);
        });
        return view('elearning::modules.create', compact('managedGroups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'image' => 'nullable|image',
            'public' => 'boolean',
            'grupo_id' => 'exists:grupos,id|nullable'
        ]);

        $module = new Module($request->except('image', 'public'));
        $module->public = $request->has('public');

        if($request->hasFile('image') && $request->file('image')->isValid()) {
            $path = $request->image->store('modules', 'public');
            $module->image = Storage::disk('public')->url($path);
        }

        $module->save();

	    noty('Módulo criado com sucesso.', 'success');

        return redirect(route('elearning.modulos.show', $module));
    }

    /**
     * Display the specified resource.
     *
     * @param Module $module
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function show(Module $module)
    {
        $lessons = $module->lessons()->published()->get();
        $notPublishedLessons = $module->lessons()->notPublished()->get();
        return view('elearning::modules.show', compact('module', 'lessons', 'notPublishedLessons'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Module  $module
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(Module $module)
    {
        $user = Auth::user();
        $managedGroups = Grupo::all()->filter(function($g) use ( $user ) {
            return $g->isManagedBy($user);
        });
        return view('elearning::modules.edit', compact('managedGroups', 'module'))->with('formModel', $module);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Module  $module
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Module $module)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'image' => 'nullable|image',
            'public' => 'boolean',
            'grupo_id' => 'exists:grupos,id|nullable'
        ]);

        $module->fill($request->except('image', 'public'));
        $module->public = $request->has('public');

        if($request->hasFile('image') && $request->file('image')->isValid()) {
            $path = $request->image->store('modules', 'public');
            $module->image = Storage::disk('public')->url($path);
        }

        $module->save();

	    noty('Módulo editado com sucesso.', 'success');

        return redirect(route('elearning.modulos.show', $module));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Module  $module
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy(Module $module)
    {
        $module->delete();

	    noty('Módulo eliminado com sucesso.', 'success');

        return redirect(route('elearning.index'));
    }

    public function grupo(Grupo $grupo)
    {
        $this->authorize('view', $grupo);

        $publicModules = Module::where('grupo_id', $grupo->id)->where('public', true)->get();
        $privateModules = Module::where('grupo_id', $grupo->id)->where('public', false)->get();

        return view('elearning::group', compact('grupo', 'publicModules', 'privateModules'));
    }
}
