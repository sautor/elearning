<?php

namespace Sautor\ELearning\Controllers;

use Sautor\Core\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Sautor\ELearning\Models\Lesson;
use Sautor\ELearning\Models\Module;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class LessonsController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Lesson::class, 'lesson');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        $modules = Module::all()->filter(function($m) use ($user) {
           return $user->can('update', $m);
        });
        $module_id = $request->query('module');
        return view('elearning::lessons.create', compact('modules', 'module_id'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'image|nullable',
            'audio' => 'file|nullable|mimetypes:audio/mpeg',
            'quiz' => 'file|nullable|mimetypes:application/json,text/plain',
            'public' => 'boolean',
            'module_id' => 'required|exists:lms_modules,id',
            'order' => 'required|integer',
            'published_at' => 'nullable|date',
            'available_at' => 'nullable|date'
        ]);

        $lesson = new Lesson($request->except(['image', 'audio', 'quiz', 'public']));
        $lesson->public = $request->has('public');

        foreach (['image', 'audio', 'quiz'] as $fileKey) {
            if($request->hasFile($fileKey) && $request->file($fileKey)->isValid()) {
                $path = $request->{$fileKey}->store('lessons', 'public');
                $lesson->{$fileKey} = Storage::disk('public')->url($path);
            }
        }

        $lesson->save();

        noty('Lição criada com sucesso.', 'success');

        return redirect(route('elearning.licoes.show', $lesson));
    }

    /**
     * Display the specified resource.
     *
     * @param  Lesson  $lesson
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show(Lesson $lesson)
    {
        $completionUser = Auth::check() ? $lesson->completedBy()->where('pessoa_id', \Auth::id())->first() : null;
        $completion = $completionUser ? $completionUser->completion : null;
        return view('elearning::lessons.show', compact('lesson', 'completion'));
    }

    /**
     * Display the lesson quiz.
     *
     * @param  Lesson  $lesson
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function showQuiz(Lesson $lesson)
    {
        $this->authorize('view', $lesson);

        if (!$lesson->quiz) {
	        noty('Esta lição não tem quiz.', 'warning');
            return redirect(route('elearning.licoes.show', $lesson));
        }
        $completionUser = Auth::check() ? $lesson->completedBy()->where('pessoa_id', \Auth::id())->first() : null;
        if (Auth::check() && !$completionUser) {
	        noty('Tens de terminar a lição antes de poderes avançar para o quiz.', 'info');
            return redirect(route('elearning.licoes.show', $lesson));
        }
        $completion = $completionUser ? $completionUser->completion : null;
        return view('elearning::lessons.quiz', compact('lesson', 'completion'));
    }

    /**
     * Store the lession completion.
     *
     * @param  Lesson  $lesson
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function storeCompletion(Lesson $lesson)
    {
        $this->authorize('view', $lesson);

        $completionUser = Auth::check() && $lesson->completedBy()->where('pessoa_id', \Auth::id())->first();
        if(!$completionUser) {
           $lesson->completedBy()->attach(Auth::id());
        }

        if($lesson->quiz) {
            return redirect(route('elearning.licoes.showQuiz', $lesson));
        }

        return redirect('elearning.modulos.show', $lesson->module);
    }

    /**
     * Store the lession quiz score.
     *
     * @param  Lesson  $lesson
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function storeScore(Request $request, Lesson $lesson)
    {
        $this->authorize('view', $lesson);

        $completionUser = Auth::check() && $lesson->completedBy()->where('pessoa_id', \Auth::id())->first();
        if(!$completionUser) {
	        noty('Tens de terminar a lição antes de poderes avançar para o quiz.', 'info');
            return redirect(route('elearning.licoes.show', $lesson));
        }

        $this->validate($request, [
            'score' => 'required|integer'
        ]);

        $lesson->completedBy()->updateExistingPivot(Auth::id(), ['score' => $request->score]);
        return redirect(route('elearning.modulos.show', $lesson->module));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Lesson  $lesson
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(Lesson $lesson)
    {
        $user = Auth::user();
        $modules = Module::all()->filter(function($m) use ($user) {
            return $user->can('update', $m);
        });
        return view('elearning::lessons.edit', compact('modules', 'lesson'))->with('formModel', $lesson);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Lesson  $lesson
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Lesson $lesson)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'image|nullable',
            'audio' => 'file|nullable|mimetypes:audio/mpeg',
            'quiz' => 'file|nullable|mimetypes:application/json,text/plain',
            'public' => 'boolean',
            'module_id' => 'required|exists:lms_modules,id',
            'order' => 'required|integer'
        ]);

        $lesson->fill($request->except(['image', 'audio', 'quiz', 'public']));
        $lesson->public = $request->has('public');

        foreach (['image', 'audio', 'quiz'] as $fileKey) {
            if($request->hasFile($fileKey) && $request->file($fileKey)->isValid()) {
                $path = $request->{$fileKey}->store('lessons', 'public');
                $lesson->{$fileKey} = Storage::disk('public')->url($path);
            }
        }

        $lesson->save();

	    noty('Lição editada com sucesso.', 'success');

        return redirect(route('elearning.licoes.show', $lesson));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Lesson  $lesson
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy(Lesson $lesson)
    {
        $module = $lesson->module;

        $lesson->delete();

	    noty('Lição eliminada com sucesso.', 'success');

        return redirect(route('elearning.modulos.show', $module));
    }

    public function addAttachment(Lesson $lesson, Request $request)
    {
        $this->authorize('update', $lesson);

        $this->validate($request, [
            'file' => 'required|file'
        ]);

        $media = $lesson->addMediaFromRequest('file');
        if(!empty($request->name)) {
            $media->usingName($request->name);
        }
        $media->toMediaCollection();

	    noty('Anexo adicionado com sucesso.', 'success');

        return back();
    }

    public function deleteAttachment(Lesson $lesson, Media $media)
    {
        $this->authorize('update', $lesson);

        $media->delete();

	    noty('Anexo eliminado com sucesso.', 'success');

        return back();
    }
}
