<?php

// Add proper web handling
Route::prefix('elearning')->middleware(['web', 'addon:elearning'])->name('elearning.')->namespace('Sautor\ELearning\Controllers')->group(function () {
    Route::redirect('/', '/elearning/modulos')->name('index');
    Route::resource('modulos', 'ModulesController')->parameter('modulos', 'module');
    Route::resource('licoes', 'LessonsController')->parameter('licoes', 'lesson')->except(['index']);
    Route::post('licoes/{lesson}/completion', 'LessonsController@storeCompletion')->name('licoes.storeCompletion');
    Route::get('licoes/{lesson}/quiz', 'LessonsController@showQuiz')->name('licoes.showQuiz');
    Route::post('licoes/{lesson}/quiz', 'LessonsController@storeScore')->name('licoes.storeScore');

    Route::post('licoes/{lesson}/anexos', 'LessonsController@addAttachment')->name('licoes.addAttachment');
    Route::delete('licoes/{lesson}/anexos/{media}', 'LessonsController@deleteAttachment')->name('licoes.deleteAttachment');

    Route::get('grupo/{grupo}', 'ModulesController@grupo')->name('grupo');
});


