<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lms_lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->string('description');
            $table->mediumText('content')->nullable();
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->string('audio')->nullable();
            $table->string('quiz')->nullable();
            $table->boolean('public')->default(false);
            $table->integer('module_id')->unsigned()->nullable();
            $table->foreign('module_id')->references('id')->on('lms_modules')->onDelete('set null');
            $table->integer('order')->unsigned()->nullable();
            $table->timestamp('published_at')->nullable();
            $table->timestamp('available_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lms_lessons');
    }
}
